<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('sales', 'SaleController');
Route::resource('suppliers', 'SupplierController');
Route::resource('products', 'ProductController');
Route::get('/', 'PagesController@getIndex');
