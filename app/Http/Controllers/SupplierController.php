<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Supplier;
use Session;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $suppliers = Supplier::orderBy('id', 'desc')->paginate(10);
        return view('suppliers.index')->withSuppliers($suppliers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
                'company_name'  => 'required|max:191',
                'first_name'    => 'required|max:191',
                'last_name'     => 'required|max:191',
                'email'         => 'required|max:191',
                'mobile_no'     => 'required|max:191',
                'address'     => 'required',
                'comments'      => 'required |max:191',
                'account'       => 'required'

            ));
        //store in database

        $supplier = new Supplier;

        $supplier->company_name = $request->company_name;
        $supplier->first_name = $request->first_name;
        $supplier->last_name = $request->last_name;
        $supplier->email = $request->email;
        $supplier->mobile_no = $request->mobile_no;
        $supplier->address = $request->address;
        $supplier->comments = $request->comments;
        $supplier->account = $request->account;

        $supplier->save();


        Session::flash('success', 'The blog post was successfully save!');

        //return redirect()->route('suppliers.show');
        return redirect()->route('suppliers.show', $supplier->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::find($id);
        return view('suppliers.show')->withSupplier($supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
