@extends('main')


@section('title','| Crerate Product')


@section('content')

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<h3>Create Product</h3>
			<hr>
			{!! Form::open(array('route' => 'products.store','class'=>'form-horizontal form-horizontal-option', 'files' => true)) !!}
			
				{{ Form::label('name', 'Name:') }}
				{{ Form::text('name', null, array('class' => 'form-control')) }}


				{{ Form::label('supplier_id', 'Supplier:') }}
				<select class="form-control" name="supplier_id">

					@foreach($suppliers as $supplier)
						<option value='{{ $supplier->id }}'>{{ $supplier->last_name }}</option>
					@endforeach
					s
				</select>

				{{ Form::submit('Create Poroduct', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!}
			
	</div>
	
</div>

@endsection