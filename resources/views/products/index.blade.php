@extends('main')


@section('title','| Show Products')


@section('content')
	
	<div class="row">
		<div class="col-md-9">
			<h3>All Products</h3>
		</div>

		<div class="col-md-3">
			<a href="{{route('products.create')}}" class="btn btn-md btn-block btn-primary btn-h1-spacing">Create New products</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Product Name</th>
					<th>Supplier Name</th>
					<th></th>
				</thead>

				<tbody>
					@foreach ($products as $product)
						<tr>
							<td>{{ $product->id }} .</td>
							<td>{{$product->name}}</td>
							<td>{{$product->supplier->last_name}}</td>
							<td><a href="{{ route('products.show', $product->id) }}" class="btn btn-default btn-sm">View</a> <a href="{{ route('products.show', $product->id) }}" class="btn btn-primary btn-sm">Edit</a> <a href="" class="btn btn-danger btn-sm">Delete</a></td>
						</tr>

					@endforeach
						
				</tbody>
			</table>
			<hr>
			<div class="text-center">
				<ul class="pagination pagination-sm">
				    <li><a href="#">1</a></li>
				    <li><a href="#">2</a></li>
				    <li><a href="#">3</a></li>
				    <li><a href="#">4</a></li>
				    <li><a href="#">5</a></li>
			    </ul>
			</div>
		</div>
	</div>

@endsection