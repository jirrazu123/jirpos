@extends('main')

@section('title', '| Show Product')

@section('content')

	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<h3>Show the product details</h3>
			<hr>
			<div class="well col-md-7">
				<h4>Product Name:{{$product->name}}</h4>
				<p>Good sells are dependent on Marketing. Marketing more sell more.</p>
				<h5>Supplied By:{{$product->supplier->last_name}}</h5>
				<hr>
			  <p><strong>Hi Everyone :</strong><i>This is testing product</i></p>
			</div>

		</div>
	</div>

@endsection

