@extends('main')

@section('title', '| Welcome page')

@section('content')
     <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <p>Welcome to your business.</p>
                    <hr />
                    <div class="panel panel-default">

                            <div class="panel-heading">
                               <h3 class="panel-title">Statistic</h3>
                            </div>

                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="well">Total Customers : </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="well">Total Products : </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="well">Total Suppliers : </div>
                                    </div>
                                </div>
                              
                                <div class="row">
                                      <div class="col-md-3">
                                        <div class="well">Total Due: </div>
                                      </div> 

                                      <div class="col-md-3">
                                        <div class="well"> : </div>
                                      </div>

                                      <div class="col-md-3">
                                        <div class="well"> : </div>
                                      </div>
                                      
                                      <div class="col-md-3">
                                        <div class="well">Total Sales : </div>
                                      </div>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
