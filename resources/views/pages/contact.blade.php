@extends('main')


@section('content')

<div class="row">
	<div class="col-md-1 col-md-offset-1">
		
	</div>
	<div class="col-md-8">
		<h3>Contact with me</h3>
                <hr>
                <form action="#" method="POST">
                   
                    <div class="form-group">
                        <label name="email">Email:</label>
                        <input id="email" name="email" class="form-control">
                    </div>

                    <div class="form-group">
                        <label name="subject">Subject:</label>
                        <input id="subject" name="subject" class="form-control">
                    </div>

                    <div class="form-group">
                        <label name="message">Message:</label>
                        <textarea id="message" name="message" class="form-control">Type your message here...</textarea>
                    </div>

                    <input type="submit" value="Send Message" class="btn btn-success">
                </form>
	</div>
	<div class="col-md-3">
		
	</div>

</div>


@endsection