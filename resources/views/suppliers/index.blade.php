@extends('main')

@section('title','| Suppliers')

@section('content')

	<div class="row">
		<div class="col-md-9">
			<h3>All Suppliers</h3>
		</div>

		<div class="col-md-3">
			<a href="{{route('suppliers.create')}}" class="btn btn-md btn-block btn-primary btn-h1-spacing">Create New Supplier</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Company Name</th>
					<th>Supplier Name</th>
					<th>E-mail</th>
					<th>Mobile No</th>
					<th>Address</th>
					<!--<th>Comments</th>-->
					<th>Account</th>
					<th>Created At</th>
					<th></th>
				</thead>

				<tbody>
					@foreach ($suppliers as $supplier)
						
						<tr>
							<th>{{ $supplier->id }}</th>
							<td>{{ $supplier->company_name }}</td>
							<td>{{ $supplier->first_name }}</td>
							<td>{{ $supplier->email }}</td>
							<td>{{ $supplier->mobile_no }}</td>
							<td>{{ $supplier->address }}</td>
							<!--<td>{{ $supplier->comments }}</td>-->
							<td>{{ $supplier->account }}</td>
							<td><a href="{{ route('suppliers.show', $supplier->id) }}" class="btn btn-default btn-sm">View</a><a href="{{ route('suppliers.show', $supplier->id) }}" class="btn btn-primary btn-sm">Edit</a><a href="" class="btn btn-danger btn-sm">Delete</a></td>
						</tr>

					@endforeach
						
				</tbody>
			</table>
			<hr>
			<div class="text-center">
				<ul class="pagination pagination-sm">
				    <li><a href="#">1</a></li>
				    <li><a href="#">2</a></li>
				    <li><a href="#">3</a></li>
				    <li><a href="#">4</a></li>
				    <li><a href="#">5</a></li>
			    </ul>
			</div>
		</div>
	</div>
@endsection