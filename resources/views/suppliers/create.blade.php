@extends('main')

@section('title','| Create supplier')

@section('content')

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h2>Create New Supplier</h2>

        <hr>
         
         {!! Form::open(array('route' => 'suppliers.store', 'class'=>'form-horizontal form-horizontal-option', 'files' => true)) !!}
            {{ Form::label('company_name','Company Name:')}}
            {{ Form::text('company_name', null, array('class'=>'form-control'))}}

            {{ Form::label('first_name','First Name:')}}
            {{ Form::text('first_name', null, array('class'=>'form-control'))}}

            {{ Form::label('last_name','Last Name:')}}
            {{ Form::text('last_name', null, array('class'=>'form-control'))}}

            {{ Form::label('email','Email:')}}
            {{ Form::text('email', null, array('class'=>'form-control'))}}

            {{ Form::label('mobile_no','Mobile No:')}}
            {{ Form::text('mobile_no', null, array('class'=>'form-control'))}}

            {{ Form::label('address','Address:')}}
            {{ Form::textarea('address', null, array('class'=>'form-control'))}}

            {{ Form::label('comments','Comments:')}}
            {{ Form::textarea('comments', null, array('class'=>'form-control'))}}

            {{ Form::label('account','Account:')}}
            {{ Form::text('account', null, array('class'=>'form-control'))}}

            {{ Form::submit('Create Supplier', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
        {!! Form::close() !!}
        
    </div>
</div>

@endsection