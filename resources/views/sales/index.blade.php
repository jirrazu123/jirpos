@extends('main')

@section('title', '| Sell product')

@section('content')

<div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-inbox" aria-hidden="true"></span> Invoice</div>

            <div class="panel-body">

                
                <div class="row" ng-controller="SearchItemCtrl">

                    <div class="col-md-3">
                        <label>Search Item:<input ng-model="searchKeyword" class="form-control"></label>

                        <table class="table table-hover">
                        <tr ng-repeat="item in items  | filter: searchKeyword | limitTo:10">

                        <td></td>
                        <td><button class="btn btn-success btn-xs" type="button" ng-click="addSaleTemp(item, newsaletemp)"><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></button></td>

                        </tr>
                        </table>
                    </div>

                    <div class="col-md-9">

                        <div class="row">
                            
                           
                                <div class="col-md-5">

                                    <div class="form-group">
                                        <label for="invoice" class="col-sm-3 control-label">Invoice No.</label>
                                        <div class="col-sm-9">
                                        <input type="text" class="form-control" id="invoice" value="" readonly/>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-7">

                                    <div class="form-group">
                                        <label for="customer_id" class="col-sm-4 control-label"> <input type="" name="">Supplier:</label>
                                        <div class="col-sm-8">
                                        
                                        </div>
                                    </div>

                                </div>
                            
                        </div>
                           
                        <table class="table table-bordered">
                            <tr>
                            	<th>Product ID</th>
                            	<th>Product Name</th>
                            	<th>Price</th>
                            	<th>Quantity</th>
                            	<th>Total</th>
                            	<th>&nbsp;</th>
                            </tr>
                            <tr ng-repeat="newsaletemp in saletemp">
                            	<td>1</td>
                            	<td>Water</td>
                            	<td>$25</td>
                            	<td>
                            		<input type="text" style="text-align:center" autocomplete="off" name="quantity" ng-change="updateSaleTemp(newsaletemp)" ng-model="newsaletemp.quantity" size="2">
                            	</td>
                            	<td>$100</td>
                            	<td>
                            		<button class="btn btn-danger btn-xs" type="button" ng-click="removeSaleTemp(newsaletemp.id)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                            	</td>
                            </tr>
                        </table>

                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total" class="col-sm-4 control-label">Add Payment</label>
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                <div class="input-group-addon">$</div>
                                                <input type="text" class="form-control" id="add_payment" ng-model="add_payment"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>&nbsp;</div>
                                    <div class="form-group">
                                        <label for="employee" class="col-sm-4 control-label">Comments</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" name="comments" id="comments" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="supplier_id" class="col-sm-4 control-label">Total :</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><b></b></p>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                            <label for="amount_due" class="col-sm-4 control-label">Due  :</label>
                                            <div class="col-sm-8">
                                            <p class="form-control-static"></p>
                                            </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success btn-block">Complete Sale</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                           
                            
                        

                    </div>

                </div>

            </div>
            </div>
        </div>
    </div>
@endsection