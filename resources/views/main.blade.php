<!doctype html>
<html lang="en">

  <head>
    @include('partials._head')

    <title>Jir Islam @yield('title')</title>
    
  </head>

<body>
    
  
    @include('partials._nav')
   
    <div class="container">

      @include('partials._messages')

      @yield('content')

      @include('partials._footer')

    </div>

    @include('partials._javascript')

</body>

</html>